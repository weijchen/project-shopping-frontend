# ShoppingAppFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.2.

## Project Features
### General
- Authentication (Login / Register)
- Authorization: separate pages based on the identity of login user
- RWD website built with Angular and Bootstrap

### Admin User
- Product Management
  - Create
  - Update
- Orders Management
  - Track details and status of each order
  - Update status of each order
- Stats information (most profitable, most popular product)

### Normal User
- Watch List
  - Add
  - Remove
- Order
  - Create
  - View past orders
  - Update order status
- Stats information (most frequently, most recently product)

### Screenshots
- Create order
![Create Order](./create-order.png)

- Create product
![Create Product](./create-product.png)

- Update product
![Update Product](./update-product.png)

- home
![Home](./home.png)

- Stats Info
![Stats Info](./stats.png)


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
