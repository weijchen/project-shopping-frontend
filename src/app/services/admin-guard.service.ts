import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth-service.service';
import { ROLE } from '../models/enums/role';

@Injectable({
  providedIn: 'root',
})
export class AdminGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}
  canActivate(): boolean {
    if (this.auth.getUserRole() === ROLE.ADMIN) {
      return true;
    }
    this.router.navigate(['/']);
    return false;
  }
}
