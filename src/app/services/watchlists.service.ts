import { Injectable } from '@angular/core';
import { PATH } from '../constants/env';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, map, tap } from 'rxjs';
import { WatchList } from '../models/interfaces/watchlist';
import { handleError } from '../utils/error';
import { Product } from '../models/interfaces/product';

@Injectable({
  providedIn: 'root',
})
export class WatchListsService {
  private watchListUrl: string = PATH.CONTENT_URL + '/watchlist';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private http: HttpClient) {}

  // GET watch list
  getWatchList(): Observable<WatchList> {
    return this.http
      .get<WatchList>(this.watchListUrl, this.httpOptions)
      .pipe(catchError(handleError<WatchList>('getWatchList')));
  }

  // POST add item to watch list
  addToWatchList(id: number): Observable<Product> {
    return this.http
      .post<Product>(this.watchListUrl + `/product/${id}`, this.httpOptions)
      .pipe(
        tap((_) => alert('Product added to watch list')),
        catchError(handleError<Product>(`addToWatchList id=${id}`))
      );
  }

  // DELETE remove item from watch list
  removeFromWatchList(id: number): Observable<Product> {
    return this.http
      .delete<Product>(this.watchListUrl + `/product/${id}`, this.httpOptions)
      .pipe(
        tap((_) => alert('Product remove from watch list')),
        catchError(handleError<Product>(`removeFromWatchList id=${id}`))
      );
  }
}
