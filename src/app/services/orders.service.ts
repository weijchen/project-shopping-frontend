import { Injectable } from '@angular/core';
import { PATH } from '../constants/env';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Order } from '../models/interfaces/order';
import { Observable, catchError, tap } from 'rxjs';
import { handleError } from '../utils/error';
import { GenericResponse } from '../models/responses/genericResponse';
import { NewOrder } from './newOrder';
import { NewOrderItem } from './newOrderItem';
import {
  INewOrderRequest,
  createNewOrderRequest,
} from '../models/requests/newOrderRequest';
import {
  INewOrderItemRequest,
  createNewOrderItemRequest,
} from '../models/requests/newOrderItemRequest';
import { ERROR_MSG } from '../models/enums/errorMsg';

@Injectable({
  providedIn: 'root',
})
export class OrdersService {
  private orderUrl: string = PATH.CONTENT_URL + '/orders';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private http: HttpClient) {}

  // GET get all orders
  getOrders(): Observable<Order[]> {
    return this.http.get<Order[]>(this.orderUrl, this.httpOptions).pipe(
      tap((_) => console.log('Get all orders')),
      catchError(handleError<Order[]>(`getOrders`))
    );
  }

  // GET order by id
  getOrder(id: number): Observable<Order> {
    const url = `${this.orderUrl}/${id}`;
    return this.http.get<Order>(url, this.httpOptions).pipe(
      tap((_) => console.log(`Get order id=${id}`)),
      catchError(handleError<Order>(`getOrder id=${id}`))
    );
  }

  // PATCH update order's status to cancel
  cancelOrder(id: number): Observable<GenericResponse> {
    const url = `${this.orderUrl}/${id}/cancel`;
    return this.http.patch<GenericResponse>(url, this.httpOptions).pipe(
      tap((res: GenericResponse) => {
        if (confirm(res.message)) {
          if (
            res.message !== ERROR_MSG.ORDER_STATUS_UPDATE_FAIL_FORBIDDEN &&
            res.message !== ERROR_MSG.ORDER_STATUS_UPDATE_FAIL_SAME
          ) {
            window.location.reload();
          }
        }
      }),
      catchError(handleError<GenericResponse>(`cancelOrder id=${id}`))
    );
  }

  // PATCH update order's status to complete
  completeOrder(id: number): Observable<GenericResponse> {
    const url = `${this.orderUrl}/${id}/complete`;
    return this.http.patch<GenericResponse>(url, this.httpOptions).pipe(
      tap((res: GenericResponse) => {
        if (confirm(res.message)) {
          if (
            res.message !== ERROR_MSG.ORDER_STATUS_UPDATE_FAIL_FORBIDDEN &&
            res.message !== ERROR_MSG.ORDER_STATUS_UPDATE_FAIL_SAME
          ) {
            window.location.reload();
          }
        }
      }),
      catchError(handleError<GenericResponse>(`completeOrder id=${id}`))
    );
  }

  // POST create new order
  createOrder(newOrder: NewOrder): Observable<GenericResponse> {
    const url = `${this.orderUrl}`;

    let newOrderRequest = createNewOrderRequest({
      newOrderItemRequestList: [],
    });
    newOrder.newOrderItems.forEach((element) => {
      let newOrderItemRequest = createNewOrderItemRequest({
        product_id: element.product_id,
        quantity: element.quantity,
      });
      newOrderRequest.newOrderItemRequestList.push(newOrderItemRequest);
    });
    return this.http
      .post<GenericResponse>(url, newOrderRequest, this.httpOptions)
      .pipe(
        tap((res: GenericResponse) => {
          if (res.message === ERROR_MSG.ORDER_CREATED) {
            localStorage.removeItem('order');
          }

          if (confirm(res.message)) {
            window.location.reload();
          }
        }),
        catchError(handleError<GenericResponse>(`createOrder`))
      );
  }

  updateCurrentOrder(id: number, title: string, amount: number, price: number) {
    let newOrder: NewOrder = {
      newOrderItems: [],
    };
    let newOrderItem: NewOrderItem = {
      product_id: id,
      title: title,
      quantity: amount,
      price: price,
    };
    if (localStorage.getItem('order') === null) {
      newOrder.newOrderItems.push(newOrderItem);
    } else {
      newOrder = JSON.parse(localStorage['order']);

      let found: boolean = false;
      for (const item of newOrder.newOrderItems) {
        if (item.product_id === id) {
          item.quantity += amount;
          found = true;
        }
      }

      if (!found) {
        newOrder.newOrderItems.push(newOrderItem);
      }
    }
    localStorage.setItem('order', JSON.stringify(newOrder));
  }
}
