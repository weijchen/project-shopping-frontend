import { NewOrderItem } from './newOrderItem';

export interface NewOrder {
  newOrderItems: NewOrderItem[];
}
