import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, of, tap } from 'rxjs';
import { Product } from '../models/interfaces/product';
import { PATH } from '../constants/env';
import { handleError } from '../utils/error';
import { MostFreqProductResponse } from '../models/responses/mostFreqProductResponse';
import { MostRecentProductResponse } from '../models/responses/mostRecentProductResponse';
import { MostProfitProductResponse } from '../models/responses/mostProfitProductResponse';
import { GenericResponse } from '../models/responses/genericResponse';
import { createUpdateProductRequest } from '../models/requests/updateProductRequest';
import { createNewProductRequest } from '../models/requests/newProductRequest';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  private productUrl: string = PATH.CONTENT_URL + '/products';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private http: HttpClient) {}

  // GET products
  getProducts(): Observable<Product[]> {
    return this.http
      .get<Product[]>(this.productUrl, this.httpOptions)
      .pipe(catchError(handleError<Product[]>('getProducts', [])));
  }

  // GET product by id
  getProduct(id: number): Observable<Product> {
    const url = `${this.productUrl}/${id}`;
    return this.http
      .get<Product>(url, this.httpOptions)
      .pipe(catchError(handleError<Product>(`getProduct id=${id}`)));
  }

  // GET get n frequently purchased products
  getMostFrequentlyPurchasedNProduct(
    n: number
  ): Observable<MostFreqProductResponse[]> {
    const url = `${this.productUrl}/frequent/${n}`;
    return this.http
      .get<MostFreqProductResponse[]>(url, this.httpOptions)
      .pipe(
        catchError(
          handleError<MostFreqProductResponse[]>(
            `getMostFrequentlyPurchasedNProduct`
          )
        )
      );
  }

  // GET get n recent purchased products
  getMostRecentPurchasedNProduct(
    n: number
  ): Observable<MostRecentProductResponse[]> {
    const url = `${this.productUrl}/recent/${n}`;
    return this.http
      .get<MostRecentProductResponse[]>(url, this.httpOptions)
      .pipe(
        catchError(
          handleError<MostRecentProductResponse[]>(
            `getMostRecentPurchasedNProduct`
          )
        )
      );
  }

  // GET get n profitable purchased products
  getMostProfitablePurchasedNProduct(
    n: number
  ): Observable<MostProfitProductResponse[]> {
    const url = `${this.productUrl}/profit/${n}`;
    return this.http
      .get<MostProfitProductResponse[]>(url, this.httpOptions)
      .pipe(
        catchError(
          handleError<MostProfitProductResponse[]>(
            `getMostProfitablePurchasedNProduct`
          )
        )
      );
  }

  // GET get n popular purchased products
  getMostPopularPurchasedNProduct(
    n: number
  ): Observable<MostFreqProductResponse[]> {
    const url = `${this.productUrl}/popular/${n}`;
    return this.http
      .get<MostFreqProductResponse[]>(url, this.httpOptions)
      .pipe(
        catchError(
          handleError<MostFreqProductResponse[]>(
            `getMostPopularPurchasedNProduct`
          )
        )
      );
  }

  addProduct(
    title: string,
    description: string,
    wholesalePrice: number,
    retailPrice: number,
    quantity: number
  ): Observable<GenericResponse> {
    let newProductRequest = createNewProductRequest({
      title,
      description,
      wholesalePrice,
      retailPrice,
      quantity,
    });

    const url = `${this.productUrl}`;
    return this.http
      .post<GenericResponse>(url, newProductRequest, this.httpOptions)
      .pipe(
        tap((res: GenericResponse) => {
          if (confirm(res.message)) {
            window.location.reload();
          }
        }),
        catchError(handleError<GenericResponse>(`createOrder`))
      );
  }

  // PATCH update product information
  updateProduct(
    id: number,
    title: string,
    description: string,
    wholesalePrice: number,
    retailPrice: number,
    quantity: number
  ): Observable<GenericResponse> {
    let newUpdateProductRequest = createUpdateProductRequest({
      title,
      description,
      wholesalePrice,
      retailPrice,
      quantity,
    });

    const url = `${this.productUrl}/${id}`;
    return this.http
      .patch<GenericResponse>(url, newUpdateProductRequest, this.httpOptions)
      .pipe(
        tap((res: GenericResponse) => {
          if (confirm(res.message)) {
            window.location.reload();
          } else {
            localStorage.removeItem('order');
          }
        }),
        catchError(handleError<GenericResponse>(`createOrder`))
      );
  }
}
