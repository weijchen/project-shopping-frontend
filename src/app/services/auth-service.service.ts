import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import jwt_decode from 'jwt-decode';

import { PATH } from '../constants/env';
import { RegisterResponse } from '../models/responses/registerResponse';
import { LoginResponse } from '../models/responses/loginResponse';
import { JWT } from '../models/interfaces/jwt';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements OnInit {
  private authUrl: string = PATH.AUTH_URL + '/auth';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  logged: any = null;
  username: any = null;
  role: any = null;
  userId: any = null;

  constructor(
    private http: HttpClient,
    private jwtHelper: JwtHelperService,
    private router: Router
  ) {
    this.loadLoggedInUser();
  }

  ngOnInit(): void {}

  private loadLoggedInUser() {
    const logged = localStorage.getItem('logged');

    if (logged) {
      this.logged = JSON.parse(logged);
      this.role = this.getUserRole();
    }
  }

  public getUserRole(): string {
    const token = localStorage.getItem('token');
    if (token) {
      let jwt = jwt_decode(token) as JWT;
      return jwt.permissions[0].authority;
    }
    return '';
  }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  login(username: string, password: string): void {
    const body = { username: username, password: password };
    this.http.post(this.authUrl + '/login', body, this.httpOptions).subscribe({
      next: (data) => {
        let ret = data as LoginResponse;

        if (ret.token !== undefined) {
          let jwt = jwt_decode(ret.token) as JWT;
          localStorage.setItem('token', ret.token);
          localStorage.setItem('logged', 'true');
          this.logged = true;
          this.router.navigate(['/']);
        } else {
          alert(ret.message);
        }
      },
      error: (error: HttpErrorResponse) => {
        alert(error.message);
      },
    });
  }

  register(username: any, password: any, email: any): void {
    const body = { username: username, password: password, email: email };
    this.http
      .post(this.authUrl + '/register', body, this.httpOptions)
      .subscribe({
        next: (data) => {
          let ret = data as RegisterResponse;
          alert(ret.message);
          this.router.navigate(['/login']);
        },
        error: (error: HttpErrorResponse) => {
          alert(error.error.message);
        },
      });
  }

  logout(): void {
    localStorage.clear();
    this.logged = false;
    this.username = null;
    this.role = null;
    this.userId = null;
    this.router.navigate(['/login']);
    alert('Logout. Welcome back next time!');
  }

  isLoggedIn(): boolean {
    return this.logged;
  }
}
