import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ROLE } from '../models/enums/role';
import { AuthService } from './auth-service.service';

@Injectable({
  providedIn: 'root',
})
export class NormalGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}
  canActivate(): boolean {
    if (this.auth.getUserRole() === ROLE.NORMAL) {
      return true;
    }
    this.router.navigate(['/']);
    return false;
  }
}
