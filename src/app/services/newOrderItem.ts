export interface NewOrderItem {
  product_id: number;
  title: string;
  quantity: number;
  price: number;
}
