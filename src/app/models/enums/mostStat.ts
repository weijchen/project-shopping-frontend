export enum MOST_STAT {
  MOST_FREQUENTLY = 'frequently',
  MOST_POPULAR = 'popular',
  MOST_PROFITABLE = 'profitable',
  MOST_RECENT = 'recent',
}
