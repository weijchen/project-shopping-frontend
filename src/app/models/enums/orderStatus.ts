export enum ORDER_STATUS {
  PROCESSING = "processing",
  COMPLETED = "completed",
  CANCELED = "canceled",
}
