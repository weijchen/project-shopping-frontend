export enum ERROR_MSG {
  ORDER_CREATED = 'Order created',
  ORDER_STATUS_UPDATE_FAIL_FORBIDDEN = 'Exception: CANCELED order cannot be COMPLETED',
  ORDER_STATUS_UPDATE_FAIL_SAME = 'Exception: status remain',
}
