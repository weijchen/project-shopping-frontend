import { Product } from "../interfaces/product";

export interface MostProfitProductResponse {
  product: Product;
  profit: number;
}
