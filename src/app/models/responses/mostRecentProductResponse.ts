import { Product } from "../interfaces/product";

export interface MostRecentProductResponse {
  product: Product;
  timestamp: string;
}
