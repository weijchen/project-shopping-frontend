import { Product } from '../interfaces/product';

export interface MostFreqProductResponse {
  product: Product;
  count: number;
}
