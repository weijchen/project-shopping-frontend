export interface INewOrderItemRequest {
  product_id: number;
  quantity: number;
}

export function createNewOrderItemRequest(config: INewOrderItemRequest): {
  product_id: number;
  quantity: number;
} {
  return {
    product_id: config.product_id,
    quantity: config.quantity,
  };
}
