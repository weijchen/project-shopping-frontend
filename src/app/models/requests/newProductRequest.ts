export interface INewProductRequest {
  title: string;
  description: string;
  wholesalePrice: number;
  retailPrice: number;
  quantity: number;
}

export function createNewProductRequest(config: INewProductRequest): {
  title: string;
  description: string;
  wholesalePrice: number;
  retailPrice: number;
  quantity: number;
} {
  return {
    title: config.title,
    description: config.description,
    wholesalePrice: config.wholesalePrice,
    retailPrice: config.retailPrice,
    quantity: config.quantity,
  };
}
