export interface IUpdateProductRequest {
  title: string;
  description: string;
  wholesalePrice: number;
  retailPrice: number;
  quantity: number;
}

export function createUpdateProductRequest(config: IUpdateProductRequest): {
  title: string;
  description: string;
  wholesalePrice: number;
  retailPrice: number;
  quantity: number;
} {
  return {
    title: config.title,
    description: config.description,
    wholesalePrice: config.wholesalePrice,
    retailPrice: config.retailPrice,
    quantity: config.quantity,
  };
}
