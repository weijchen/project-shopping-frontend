import { INewOrderItemRequest } from './newOrderItemRequest';

export interface INewOrderRequest {
  newOrderItemRequestList: INewOrderItemRequest[];
}

export function createNewOrderRequest(config: INewOrderRequest): {
  newOrderItemRequestList: INewOrderItemRequest[];
} {
  return {
    newOrderItemRequestList: config.newOrderItemRequestList,
  };
}
