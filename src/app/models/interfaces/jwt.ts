export interface JWT {
  permissions: [{ authority: string }];
  sub: string;
  userId: number;
}
