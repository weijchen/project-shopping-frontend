import { Product } from "./product";

export interface OrderItem {
  id: number;
  purchasePrice: number;
  wholesaleAtPurchase: number;
  quantity: number;
  product_id: number;
  product: Product;
}
