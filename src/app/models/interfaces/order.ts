import { OrderItem } from './orderItem';

export interface Order {
  id: number;
  placementTime: string;
  status: string;
  user_id: number;
  orderItemList: OrderItem[];
}
