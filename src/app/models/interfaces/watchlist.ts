import { Product } from './product';

export interface WatchList {
  id: number;
  user_id: number;
  watchItemList: Product[];
}
