export interface Product {
  id: number;
  title: string;
  description: string;
  wholesalePrice: number;
  retailPrice: number;
  quantity: number;
}

export function createProduct(config: Product): {
  id: number;
  title: string;
  description: string;
  wholesalePrice: number;
  retailPrice: number;
  quantity: number;
} {
  return {
    id: config.id,
    title: config.title,
    description: config.description,
    wholesalePrice: config.wholesalePrice,
    retailPrice: config.retailPrice,
    quantity: config.quantity,
  };
}
