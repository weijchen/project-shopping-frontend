import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/models/interfaces/product';
import { ROLE } from 'src/app/models/enums/role';
import { AuthService } from 'src/app/services/auth-service.service';
import { ProductsService } from 'src/app/services/products.service';
import { WatchListsService } from 'src/app/services/watchlists.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css'],
})
export class ProductDetailComponent implements OnInit {
  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.getProductDetail(id);
  }

  product: Product = {
    id: 0,
    title: '',
    description: '',
    wholesalePrice: 0,
    retailPrice: 0,
    quantity: 0,
  };

  role: any;
  ROLE = ROLE;

  constructor(
    private productService: ProductsService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private watchListService: WatchListsService
  ) {
    this.role = authService.getUserRole();
  }

  getProductDetail(id: number) {
    this.productService
      .getProduct(id)
      .subscribe((product) => (this.product = product));
  }

  addToWatchList(id: number): void {
    this.watchListService.addToWatchList(id).subscribe();
  }

  updateProduct(id: number) {
    this.productService
      .updateProduct(
        id,
        this.product.title,
        this.product.description,
        this.product.wholesalePrice,
        this.product.retailPrice,
        this.product.quantity
      )
      .subscribe();
  }
}
