import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import { Product, createProduct } from '../../models/interfaces/product';
import { OrdersService } from 'src/app/services/orders.service';
import { WatchListsService } from 'src/app/services/watchlists.service';
import { ROLE } from 'src/app/models/enums/role';
import { AuthService } from 'src/app/services/auth-service.service';
import { Order } from 'src/app/models/interfaces/order';
import { ORDER_STATUS } from 'src/app/models/enums/orderStatus';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit {
  ngOnInit(): void {
    console.log('here');

    this.getProducts();
    this.getOrders();
  }

  products: Product[] = [];
  orders: Order[] = [];
  ORDER_STATUS = ORDER_STATUS;
  role: any;
  ROLE = ROLE;

  constructor(
    private productService: ProductsService,
    public watchListService: WatchListsService,
    public orderService: OrdersService,
    private authService: AuthService
  ) {
    this.role = authService.getUserRole();
  }

  getProducts(): void {
    this.productService.getProducts().subscribe((products) => {
      this.products = products;
    });
  }

  getOrders(): void {
    this.orderService.getOrders().subscribe((orders) => {
      this.orders = orders.reverse().slice(0, 5);
    });
  }

  addToWatchList(id: number): void {
    this.watchListService.addToWatchList(id).subscribe();
  }

  parsePlacementTime(time: string) {
    return time.slice(0, 10) + ' ' + time.slice(11, 19);
  }

  cancelOrder(id: number) {
    this.orderService.cancelOrder(id).subscribe();
  }

  completeOrder(id: number) {
    this.orderService.completeOrder(id).subscribe();
  }
}
