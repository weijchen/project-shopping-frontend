import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/models/interfaces/order';
import { AuthService } from 'src/app/services/auth-service.service';
import { OrdersService } from 'src/app/services/orders.service';
import { ORDER_STATUS } from 'src/app/models/enums/orderStatus';
import { ROLE } from 'src/app/models/enums/role';
import { NewOrder } from 'src/app/services/newOrder';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
})
export class OrdersComponent implements OnInit {
  ngOnInit(): void {
    this.getOrders();
  }

  orders: Order[] = [];
  role: any;
  ORDER_STATUS = ORDER_STATUS;
  ROLE = ROLE;
  showCurrent: boolean;
  currentOrder: NewOrder = {
    newOrderItems: [],
  };
  newOrderStatus: string = 'Processing';

  constructor(
    private orderService: OrdersService,
    private authService: AuthService
  ) {
    this.role = authService.getUserRole();
    this.updateCurrentOrder();
    this.showCurrent = true;
  }

  getOrders(): void {
    this.orderService.getOrders().subscribe((orders) => (this.orders = orders.reverse()));
  }

  parsePlacementTime(time: string) {
    return time.slice(0, 10) + ' ' + time.slice(11, 19);
  }

  cancelOrder(id: number) {
    this.orderService.cancelOrder(id).subscribe();
  }

  completeOrder(id: number) {
    this.orderService.completeOrder(id).subscribe();
  }

  onOrderSwitch(toState: boolean) {
    this.showCurrent = toState;
  }

  updateCurrentOrder() {
    const order = localStorage.getItem('order');
    if (order) {
      let currentOrder = JSON.parse(order) as NewOrder;
      this.newOrderStatus =
        currentOrder.newOrderItems.length === 0 ? 'Empty' : 'Processing';
      this.currentOrder = currentOrder;
    } else {
      this.newOrderStatus = 'Empty';
    }
  }

  removeItemInCurrentOrder(id: number) {
    const order = localStorage.getItem('order');
    if (order) {
      this.currentOrder.newOrderItems = this.currentOrder.newOrderItems.filter(
        (ele) => ele.product_id !== id
      );

      localStorage.setItem('order', JSON.stringify(this.currentOrder));
      window.location.reload();
    }
  }

  createNewOrder() {
    const order = localStorage.getItem('order');
    if (order) {
      let currentOrder = JSON.parse(order) as NewOrder;
      this.orderService.createOrder(currentOrder).subscribe();
    }
  }
}
