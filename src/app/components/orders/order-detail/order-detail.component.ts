import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JWT } from 'src/app/models/interfaces/jwt';
import { Order } from 'src/app/models/interfaces/order';
import { OrderItem } from 'src/app/models/interfaces/orderItem';
import { ORDER_STATUS } from 'src/app/models/enums/orderStatus';
import { ROLE } from 'src/app/models/enums/role';
import { AuthService } from 'src/app/services/auth-service.service';
import { OrdersService } from 'src/app/services/orders.service';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css'],
})
export class OrderDetailComponent implements OnInit {
  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.getOrderDetail(id);
  }

  order: Order = {
    id: 0,
    placementTime: '',
    status: '',
    user_id: 0,
    orderItemList: [],
  };

  role: any;
  ORDER_STATUS = ORDER_STATUS;
  ROLE = ROLE;

  constructor(
    private orderService: OrdersService,
    private route: ActivatedRoute,
    private authService: AuthService
  ) {
    this.role = authService.getUserRole();
  }

  getOrderDetail(id: number) {
    this.orderService.getOrder(id).subscribe((order) => {
      this.order = order;
    });
  }

  parsePlacementTime(time: string) {
    return time.slice(0, 10) + ' ' + time.slice(11, 19);
  }

  calculateOrderTotal() {
    let sum = 0;
    for (const item of this.order.orderItemList) {
      sum += item.purchasePrice * item.quantity;
    }
    return sum;
  }

  cancelOrder(id: number) {
    this.orderService.cancelOrder(id).subscribe();
  }

  completeOrder(id: number) {
    this.orderService.completeOrder(id).subscribe();
  }
}
