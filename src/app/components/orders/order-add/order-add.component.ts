import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/models/interfaces/product';
import { ROLE } from 'src/app/models/enums/role';
import { AuthService } from 'src/app/services/auth-service.service';
import { OrdersService } from 'src/app/services/orders.service';
import { ProductsService } from 'src/app/services/products.service';
import { WatchListsService } from 'src/app/services/watchlists.service';

@Component({
  selector: 'app-order-add',
  templateUrl: './order-add.component.html',
  styleUrls: ['./order-add.component.css'],
})
export class OrderAddComponent {
  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.getProductDetail(id);
  }

  product: Product = {
    id: 0,
    title: '',
    description: '',
    wholesalePrice: 0,
    retailPrice: 0,
    quantity: 0,
  };

  role: any;
  ROLE = ROLE;
  amount: number = 0;

  constructor(
    private productService: ProductsService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private orderService: OrdersService,
    private router: Router
  ) {
    this.role = authService.getUserRole();
  }

  getProductDetail(id: number) {
    this.productService
      .getProduct(id)
      .subscribe((product) => (this.product = product));
  }

  addToOrder(id: number, title: string, amount: number, price: number): void {
    if (typeof amount !== 'number') return;

    this.orderService.updateCurrentOrder(id, title, amount, price);
    this.router.navigate(['orders']);
  }
}
