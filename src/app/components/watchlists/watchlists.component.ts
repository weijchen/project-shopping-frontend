import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { WatchList } from 'src/app/models/interfaces/watchlist';
import { WatchListsService } from 'src/app/services/watchlists.service';

@Component({
  selector: 'app-watchlists',
  templateUrl: './watchlists.component.html',
  styleUrls: ['./watchlists.component.css'],
})
export class WatchListsComponent implements OnInit {
  ngOnInit(): void {
    this.getWatchList();
  }

  watchList: WatchList = {
    id: 0,
    user_id: 0,
    watchItemList: [],
  };

  constructor(private watchListService: WatchListsService) {}

  getWatchList(): void {
    this.watchListService
      .getWatchList()
      .subscribe((watchList) => (this.watchList = watchList));
  }

  addToWatchList(id: number): void {
    this.watchListService
      .addToWatchList(id)
      .subscribe((product) => this.watchList.watchItemList.push(product));
  }

  removeFromWatchList(id: number): void {
    this.watchList.watchItemList = this.watchList.watchItemList.filter(
      (ele) => ele.id !== id
    );
    this.watchListService.removeFromWatchList(id).subscribe();
  }
}
