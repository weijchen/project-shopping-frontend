import { Component, OnInit } from '@angular/core';
import { MostFreqProductResponse } from 'src/app/models/responses/mostFreqProductResponse';
import { MostProfitProductResponse } from 'src/app/models/responses/mostProfitProductResponse';
import { MostRecentProductResponse } from 'src/app/models/responses/mostRecentProductResponse';
import { MOST_STAT } from 'src/app/models/enums/mostStat';
import { ROLE } from 'src/app/models/enums/role';
import { AuthService } from 'src/app/services/auth-service.service';
import { ProductsService } from 'src/app/services/products.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css'],
})
export class StatsComponent implements OnInit {
  ngOnInit(): void {}

  mostNProduct:
    | MostFreqProductResponse[]
    | MostProfitProductResponse[]
    | MostRecentProductResponse[] = [];

  currentStat: string = '';
  MOST_STAT = MOST_STAT;
  role: any;
  ROLE = ROLE;
  nProduct: number = 3;

  constructor(
    private productService: ProductsService,
    private authService: AuthService,
    private router: Router
  ) {
    this.role = authService.getUserRole();
  }

  setCurrentStat(toStat: string) {
    this.currentStat = toStat;
    this.mostNProduct = [];
  }

  asMostFreq(val: any): MostFreqProductResponse[] {
    return val;
  }

  asMostProfit(val: any): MostProfitProductResponse[] {
    return val;
  }

  asMostRecent(val: any): MostRecentProductResponse[] {
    return val;
  }

  updateMostList() {
    switch (this.currentStat) {
      case MOST_STAT.MOST_FREQUENTLY:
        this.getMostFrequentlyPurchasedNProduct(this.nProduct);
        break;
      case MOST_STAT.MOST_RECENT:
        this.getMostRecentPurchasedNProduct(this.nProduct);
        break;
      case MOST_STAT.MOST_PROFITABLE:
        this.getMostProfitablePurchasedNProduct(this.nProduct);
        break;
      case MOST_STAT.MOST_POPULAR:
        this.getMostPopularPurchasedNProduct(this.nProduct);
        break;
      default:
        break;
    }
  }

  getMostFrequentlyPurchasedNProduct(n: number): void {
    this.productService
      .getMostFrequentlyPurchasedNProduct(n)
      .subscribe((products) => (this.mostNProduct = products));
  }

  getMostPopularPurchasedNProduct(n: number): void {
    this.productService
      .getMostPopularPurchasedNProduct(n)
      .subscribe((products) => (this.mostNProduct = products));
  }

  getMostProfitablePurchasedNProduct(n: number): void {
    this.productService
      .getMostProfitablePurchasedNProduct(n)
      .subscribe((products) => (this.mostNProduct = products));
  }

  getMostRecentPurchasedNProduct(n: number): void {
    this.productService
      .getMostRecentPurchasedNProduct(n)
      .subscribe((products) => (this.mostNProduct = products));
  }

  routToProduct(id: number) {
    this.router.navigate([`/products/${id}`]);
  }
}
