import { Component, OnInit } from '@angular/core';
import { ORDER_STATUS } from 'src/app/models/enums/orderStatus';
import { ROLE } from 'src/app/models/enums/role';
import { Order } from 'src/app/models/interfaces/order';
import { AuthService } from 'src/app/services/auth-service.service';
import { OrdersService } from 'src/app/services/orders.service';
import { ProductsService } from 'src/app/services/products.service';
import { WatchListsService } from 'src/app/services/watchlists.service';

@Component({
  selector: 'app-orders-management',
  templateUrl: './orders-management.component.html',
  styleUrls: ['./orders-management.component.css'],
})
export class OrdersManagementComponent implements OnInit {
  ngOnInit(): void {
    this.getOrders();
  }

  orders: Order[] = [];
  ORDER_STATUS = ORDER_STATUS;
  role: any;
  ROLE = ROLE;

  constructor(
    public watchListService: WatchListsService,
    public orderService: OrdersService,
    private authService: AuthService
  ) {
    this.role = authService.getUserRole();
  }

  getOrders(): void {
    this.orderService.getOrders().subscribe((orders) => {
      this.orders = orders.reverse();
    });
  }

  addToWatchList(id: number): void {
    this.watchListService.addToWatchList(id).subscribe();
  }

  parsePlacementTime(time: string) {
    return time.slice(0, 10) + ' ' + time.slice(11, 19);
  }

  cancelOrder(id: number) {
    this.orderService.cancelOrder(id).subscribe();
  }

  completeOrder(id: number) {
    this.orderService.completeOrder(id).subscribe();
  }
}
