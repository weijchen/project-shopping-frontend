import { Component, OnInit } from '@angular/core';
import { ROLE } from 'src/app/models/enums/role';
import { AuthGuardService } from 'src/app/services/auth-guard.service';
import { AuthService } from 'src/app/services/auth-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  ngOnInit(): void {}

  ROLE = ROLE;

  constructor(public authService: AuthService) {}

  getRole() {
    return this.authService.getUserRole();
  }

  onLogout() {
    this.authService.logout();
  }
}
