import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/services/auth-service.service';
import { emptyValidator } from 'src/app/utils/empty.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  ngOnInit(): void {}

  registerForm = this.fb.group({
    username: ['', emptyValidator()],
    email: ['', emptyValidator()],
    password: ['', emptyValidator()],
  });

  constructor(private fb: FormBuilder, private authService: AuthService) {}

  onClick(): void {
    if (!this.registerForm.valid) alert('Fields cannot be empty!');
    this.authService.register(
      this.registerForm.value.username,
      this.registerForm.value.password,
      this.registerForm.value.email
    );
  }
}
