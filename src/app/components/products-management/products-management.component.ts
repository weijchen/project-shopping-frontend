import { Component, OnInit } from '@angular/core';
import { ORDER_STATUS } from 'src/app/models/enums/orderStatus';
import { PRODUCT_MGM_TYPE } from 'src/app/models/enums/productManagementType';
import { ROLE } from 'src/app/models/enums/role';
import { Product, createProduct } from 'src/app/models/interfaces/product';
import { AuthService } from 'src/app/services/auth-service.service';
import { ProductsService } from 'src/app/services/products.service';
import { WatchListsService } from 'src/app/services/watchlists.service';

@Component({
  selector: 'app-products-management',
  templateUrl: './products-management.component.html',
  styleUrls: ['./products-management.component.css'],
})
export class ProductsManagementComponent implements OnInit {
  ngOnInit(): void {
    this.getProducts();
  }

  products: Product[] = [];
  ORDER_STATUS = ORDER_STATUS;
  role: any;
  ROLE = ROLE;
  PRODUCT_MGM_TYPE = PRODUCT_MGM_TYPE;
  newProduct: Product = {
    id: 0,
    title: '',
    description: '',
    wholesalePrice: 0,
    retailPrice: 0,
    quantity: 0,
  };

  constructor(
    private productService: ProductsService,
    public watchListService: WatchListsService,
    private authService: AuthService
  ) {
    this.role = authService.getUserRole();
  }

  createDefaultProduct() {
    let product = createProduct({
      id: 0,
      title: '[ADD PRODUCT]',
      description: '[ADD PRODUCT]',
      wholesalePrice: 0,
      retailPrice: 0,
      quantity: 0,
    });
    this.products.push(product);
  }

  createNewProduct() {
    if (
      this.newProduct.title.length === 0) {
      alert('Product title is empty');
      return;
    }
    if (
      this.newProduct.retailPrice === 0 ||
      this.newProduct.retailPrice === 0
    ) {
      alert('Product price cannot be 0');
      return;
    }
    if (this.newProduct.quantity === 0) {
      alert('Product quantity cannot be 0');
      return;
    }
    this.productService
      .addProduct(
        this.newProduct.title,
        this.newProduct.description,
        this.newProduct.wholesalePrice,
        this.newProduct.retailPrice,
        this.newProduct.quantity
      )
      .subscribe();
  }

  getProducts(): void {
    this.productService.getProducts().subscribe((products) => {
      this.createDefaultProduct();
      this.products = [...this.products, ...products];
    });
  }

  parsePlacementTime(time: string) {
    return time.slice(0, 10) + ' ' + time.slice(11, 19);
  }
}
