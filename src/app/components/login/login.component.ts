import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { emptyValidator } from 'src/app/utils/empty.validator';
import { LoginResponse } from 'src/app/models/responses/loginResponse';
import { AuthService } from 'src/app/services/auth-service.service';
import { Observable, catchError, map, of, tap, throwError } from 'rxjs';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  ngOnInit(): void {}

  loginForm = {
    username: '',
    password: '',
  };

  constructor(private authService: AuthService) {}

  onClick() {
    this.authService.login(this.loginForm.username, this.loginForm.password);
  }
}
