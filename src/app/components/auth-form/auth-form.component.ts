import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { emptyValidator } from '../../utils/empty.validator';

@Component({
  selector: 'app-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.css'],
})
export class AuthFormComponent implements OnInit {
  templateForm = {
    username: '',
    email: '',
    password: '',
  };

  reactiveForm = new FormGroup({
    username: new FormControl('', emptyValidator()),
    email: new FormControl('', emptyValidator()),
    password: new FormControl('', emptyValidator()),
  });

  fBuilder = new FormBuilder().group({
    username: ['', emptyValidator()],
    email: ['', emptyValidator()],
    password: '',
  });

  constructor() {}

  ngOnInit(): void {}

  onClick() {
    console.log(this.fBuilder.getRawValue());
  }
}
