import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from '../components/not-found/not-found.component';
import { RegisterComponent } from '../components/register/register.component';
import { LoginComponent } from '../components/login/login.component';
import { AuthGuardService } from '../services/auth-guard.service';
import { LoginGuardService } from '../services/login-guard.service';
import { ProductsComponent } from '../components/products/products.component';
import { ProductDetailComponent } from '../components/products/product-detail/product-detail.component';
import { OrdersComponent } from '../components/orders/orders.component';
import { OrderDetailComponent } from '../components/orders/order-detail/order-detail.component';
import { WatchListsComponent } from '../components/watchlists/watchlists.component';
import { OrderAddComponent } from '../components/orders/order-add/order-add.component';
import { StatsComponent } from '../components/stats/stats.component';
import { ProductsManagementComponent } from '../components/products-management/products-management.component';
import { OrdersManagementComponent } from '../components/orders-management/orders-management.component';
import { NormalGuardService } from '../services/normal-guard.service';
import { AdminGuardService } from '../services/admin-guard.service';

const routes: Routes = [
  { path: '', component: ProductsComponent, canActivate: [AuthGuardService] },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LoginGuardService],
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [LoginGuardService],
  },
  {
    path: 'products/:id',
    component: ProductDetailComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'orders',
    component: OrdersComponent,
    canActivate: [AuthGuardService, NormalGuardService],
  },
  {
    path: 'orders/:id',
    component: OrderDetailComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'orders/add/:id',
    component: OrderAddComponent,
    canActivate: [AuthGuardService, NormalGuardService],
  },
  {
    path: 'watchlist',
    component: WatchListsComponent,
    canActivate: [AuthGuardService, NormalGuardService],
  },
  {
    path: 'admin/products',
    component: ProductsManagementComponent,
    canActivate: [AuthGuardService, AdminGuardService],
  },
  {
    path: 'admin/orders',
    component: OrdersManagementComponent,
    canActivate: [AuthGuardService, AdminGuardService],
  },
  {
    path: 'stats',
    component: StatsComponent,
    canActivate: [AuthGuardService],
  },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
