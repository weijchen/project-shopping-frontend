import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { AppComponent } from './app.component';
import { ProductsComponent } from './components/products/products.component';
import { AuthFormComponent } from './components/auth-form/auth-form.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HeaderComponent } from './components/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InterceptorService } from './services/interceptor.service';
import { AuthGuardService } from './services/auth-guard.service';
import { ProductDetailComponent } from './components/products/product-detail/product-detail.component';
import { WatchListsComponent } from './components/watchlists/watchlists.component';
import { OrdersComponent } from './components/orders/orders.component';
import { OrderDetailComponent } from './components/orders/order-detail/order-detail.component';
import { OrderAddComponent } from './components/orders/order-add/order-add.component';
import { StatsComponent } from './components/stats/stats.component';
import { ProductsManagementComponent } from './components/products-management/products-management.component';
import { OrdersManagementComponent } from './components/orders-management/orders-management.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    AuthFormComponent,
    NotFoundComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    ProductDetailComponent,
    WatchListsComponent,
    OrdersComponent,
    OrderDetailComponent,
    OrderAddComponent,
    StatsComponent,
    ProductsManagementComponent,
    OrdersManagementComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    HttpClientModule,
    MatCardModule,
    MatTableModule,
  ],
  providers: [
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    JwtHelperService,
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    AuthGuardService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
